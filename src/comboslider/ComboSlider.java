package comboslider;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * ComboSlider implements a combination slider/text field for entering integer
 * data. The slider and text field are linked: both display the same value,
 * and changing either one causes the other to mirror the change.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @version 1.0.1
 *
 * Licensed under the Eclipse Public License 1.0
 * (http://www.eclipse.org/legal/epl-v10.html).
 */
public class ComboSlider extends JPanel implements Runnable {
  // defeulat dimensions
  private static final int DEFAULTWIDTH = 200;
  private static final int DEFAULTHEIGHT = 47;
  private static final int DEFAULTEXTTWIDTH = 42;
  private static final int DEFAULTTEXTHEIGHT = 24;
  private static final double DEFAULTXWEIGHT = 0.5;
  private static final int DEFAULTTOPINSET = 5;
  private static final int DEFAULTDOMAINLO = 0;
  private static final int DEFAULTDOMAINHI = 100;

  private final JSlider slider;
  private final JTextField text;
  private boolean semaphore;  // signals an internal adjustment v. user action
  private int low;            // low end of the valid input range
  private int high;           // high end of the valid input range

  /**
   * Constructor.
   * @param lo the lower end of the range of valid inputs
   * @param hi the upper end of the range of valid inputs
   */
  public ComboSlider(final int lo, final int hi) {
    // initialize the panel containing the controls
    super(new GridBagLayout());
    this.setPreferredSize(new Dimension(DEFAULTWIDTH, DEFAULTHEIGHT));
    // record the low and high values
    low = lo;
    high = hi;
    // add a standard slider (from low to high) with initial value low
    slider = new JSlider(low, high, low);
    // assign this instance as a change listener to the slider
    slider.addChangeListener(new CSChangeListener());
    // set the slider location/dimensions
    GridBagConstraints gs = new GridBagConstraints();
    gs.gridx = 0;
    gs.gridy = 0;
    gs.gridwidth = 1;
    gs.gridheight = 1;
    gs.fill = GridBagConstraints.HORIZONTAL;
    gs.anchor = GridBagConstraints.WEST;
    gs.weightx = 1.0;
    this.add(slider, gs);
    // add a matching text field with low as initial text, right justified
    text = new JTextField(low);
    text.setHorizontalAlignment(JTextField.RIGHT);
    // assign this instance as a validating document listener to the text field
    text.getDocument().addDocumentListener(new CSDocListener());
    // make the minimum and preferred sizes sufficient
    text.setPreferredSize(new Dimension(DEFAULTEXTTWIDTH, DEFAULTTEXTHEIGHT));
    text.setMinimumSize(new Dimension(DEFAULTEXTTWIDTH, DEFAULTTEXTHEIGHT));
    // set the text location/dimensions
    GridBagConstraints gt = new GridBagConstraints();
    gt.gridx = 1;
    gt.gridy = 0;
    gt.gridwidth = 1;
    gt.gridheight = 1;
    gt.fill = GridBagConstraints.HORIZONTAL;
    gt.anchor = GridBagConstraints.EAST;
    gt.weightx = DEFAULTXWEIGHT;
    gt.insets = new Insets(0, DEFAULTTOPINSET, 0, 0);
    this.add(text, gt);
    semaphore = false;
  }

  /**
   * Default constructor (domain 0 to 100).
   */
  public ComboSlider() {
    this(DEFAULTDOMAINLO, DEFAULTDOMAINHI);
  }

  /**
   * Verify that a string represents an integer between 'low' and 'high'.
   * @param s the string to validate
   * @return the integer equivalent (null if invalid)
   */
  private Integer validate(final String s) {
    Integer i;
    try {
      i = Integer.valueOf(s);
    } catch (NumberFormatException e) {
      // schedule a repair of the text field
      SwingUtilities.invokeLater(this);
      return null;
    }
    if (i >= low && i <= high) {
      return i;
    } else {
      // schedule a repair of the text field
      SwingUtilities.invokeLater(this);
      return null;
    }
  }

  /**
   * Gets the value of the control.
   * @return the current value
   */
  public final int getValue() {
    return slider.getValue();
  }

  /**
   * Sets the value of the control.
   * @param v the new value
   */
  public final void setValue(final Integer v) {
    int vv = (v == null) ? 0 : v;  // catch null values
    slider.setValue(vv);
    text.setText(Integer.toString(vv));
  }

  /**
   * Fix the text field after a bad input.
   */
  @Override
  public final void run() {
    semaphore = true;
    text.setText(Integer.toString(slider.getValue()));
  }

  /**
   * Add a change listener to the slider's state.
   * @param cl the new change listener
   */
  public final void addChangeListener(final ChangeListener cl) {
    slider.addChangeListener(cl);
  }

  /**
   * Enabled or disable the control.
   * @param state true for enabled, false for disabled
   */
  @Override
  public final void setEnabled(final boolean state) {
    this.slider.setEnabled(state);
    this.text.setEnabled(state);
  }

  /**
   * Get the lower limit of the input range.
   * @return the lower limit
   */
  public final int getLow() {
    return low;
  }

  /**
   * Set the lower limit of the input range.
   * @param lo the new lower limit
   */
  public final void setLow(final int lo) {
    this.low = lo;
    slider.setMinimum(low);
    // if low > high, set them equal
    if (low > high) {
      high = low;
      slider.setMaximum(high);
    }
    // if the value is now out of range, set it to the new low
    if (slider.getValue() < low) {
      slider.setValue(low);
    }
  }

  /**
   * Get the upper limit of the input range.
   * @return the upper limit
   */
  public final int getHigh() {
    return high;
  }

  /**
   * Set the upper limit of the input range.
   * @param hi the new upper limit.
   */
  public final void setHigh(final int hi) {
    this.high = hi;
    slider.setMaximum(high);
    // if high < low, set low = high
    if (high < low) {
      low = high;
      slider.setMinimum(low);
    }
    // if the value is now out of range, set it to the new high
    if (slider.getValue() > high) {
      slider.setValue(high);
    }
  }

  /**
   * CSChangeListener provides a change listener for a ComboSlider.
   */
  private class CSChangeListener implements ChangeListener {

    /**
     * Process a change to the slider, updating the text field.
     * @param e the change event
     */
    @Override
    public final void stateChanged(final ChangeEvent e) {
      // don't react to a change making a round trip
      if (semaphore) {
        semaphore = false;
      } else {
        semaphore = true;
        // update the text field
        text.setText(((Integer) slider.getValue()).toString());
      }
    }

  }

  /**
   * CSDocListener provides a document listener for a ComboSlider.
   */
  private class CSDocListener implements DocumentListener {

    /**
     * Process an insertion into the text field, validating the input and
     * updating the slider.
     * @param e the update event
     */
    @Override
    public final void insertUpdate(final DocumentEvent e) {
      // don't react to a change making a round trip
      if (semaphore) {
        semaphore = false;
        return;
      }
      // validate the input
      Integer i = validate(text.getText());
      if (i != null) {
        semaphore = true;
        // update the slider
        slider.setValue(i);
      }
    }

    /**
     * Process a deletion from the text field, validating the input and
     * updating the slider.
     * @param e the update event
     */
    @Override
    public final void removeUpdate(final DocumentEvent e) {
      // don't react to a change making a round trip
      // also don't clear the semaphore -- an insert will follow
      // (slider updates to the text field are paired delete/insert
      // combinations)
      if (semaphore) {
        return;
      }
      Integer i;
      if (text.getText().length() == 0) {
        // if the text was deleted completely, treat that as entering the
        // low value
        i = low;
      } else {
        // otherwise, validate the input
        i = validate(text.getText());
      }
      if (i != null) {
        semaphore = true;
        slider.setValue(i);
      }
    }

    /**
     * Process a change to the text field, validating the input and
     * updating the slider.
     * @param e the update event
     */
    @Override
    public final void changedUpdate(final DocumentEvent e) {
      // don't react to a change making a round trip
      if (semaphore) {
        semaphore = false;
        return;
      }
      // validate the input
      Integer i = validate(text.getText());
      if (i != null) {
        semaphore = true;
        // update the slider
        slider.setValue(i);
      }
    }
  }

}