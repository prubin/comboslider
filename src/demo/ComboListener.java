package demo;

import comboslider.ComboSlider;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * ComboListener listens to changes to the ComboSlider and updates a label
 * in the display accordingly.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @version 1.0.1
 */
public class ComboListener implements ChangeListener {
  private final JLabel label;       // the control to update
  private final ComboSlider combo;  // the control to which to listen

  /**
   * Constructor.
   * @param cs the combo slider to monitor
   * @param lab the label to update
   */
  public ComboListener(final ComboSlider cs, final JLabel lab) {
    combo = cs;
    label = lab;
  }

  /**
   * React to a change in the ComboSlider.
   * @param e the change event (ignored)
   */
  @Override
  public final void stateChanged(final ChangeEvent e) {
    // get the new slider value
    int value = combo.getValue();
    // update the label
    label.setText("The current combo slider value is " + value);
  }

}