ComboSlider
===========

**ComboSlider** is a custom control for use with the Java Swing GUI toolkit. It
combines a slider and a text input field to provide a way for users to input
integer values. The controls are positioned side by side and linked: moving the
slider updates the text field, and editing the text field updates the slider.
(If an illegal value is entered into the text field, the value is ignored.)

ComboSlider can be used like any Swing control. In the NetBeans IDE, you
can add a ComboSlider to your GUI as follows:

* Add the `ComboSlider.jar` file to your project as a library file.
* Make sure that "Beans" is enabled in your NetBeans Palette (go to
`Tools > Palette > Swing/AWT Components` if you need to turn it on).
* In the Design screen for your parent container, right-click and select
`Add From Palette > Beans > Choose Bean` and type the full class name, including
the package (`comboslider.ComboSlider`).

Two packages are included in this distribution.

* The *comboslider* package contains the ComboSlider class.

* The *demo* package contains a small program that demonstrates the use
of a ComboSlider, including how to listen to it for changes.

Please see the Javadoc package details of the respective packages for
 additional information.

Author: Paul A. Rubin [parubin73@gmail.com](mailto:parubin73@gmail.com)

Version: 1.0.1

License: Eclipse Public License 1.0
[http://www.eclipse.org/legal/epl-v10.html](http://www.eclipse.org/legal/epl-v10.html)

Change Log
----------

Version 1.0.1:

* The slider and text field now adjust horizontally when the ComboSlider is
resized (with hard-coded weights 1.0 and 0.5 respectively).

* The demo now allows the ComboSlider to fill horizontally when the dialog is
resized.
